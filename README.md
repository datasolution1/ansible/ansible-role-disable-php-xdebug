# Ansible Role: Disable PHP-XDebug

Disable XDebug but keep a copy of the INI settings to be able to enable it back quickly if needed.

Ansible-role-php-xdebug needs to be set prior to this one in the playbooks.yml for the INI file to be available. Once the VM is up, XDebug will be disabled by default.

**To enable it back, use the `xdebug-toggle` command defined in devops-cli.sh** (from the magento-devops repository).